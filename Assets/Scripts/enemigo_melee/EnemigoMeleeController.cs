﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMeleeController : MonoBehaviour
{
	public float speed;
	public int lifes;
	public float rangeAttak;
	public GameObject rangeAttakVisual;
	//Animator animator;
	public PlayerMovement player;
	bool changueMovement = true;
    public GameObject shoot;
	// Start is called before the first frame update
	void Start()
	{
		rangeAttakVisual.GetComponent<CircleCollider2D>().radius = rangeAttak;

		animator = player.gameObject.GetComponent<Animator>();


	}

	// Update is called once per frame
	void Update()
	{
		Debug.Log(player.isAttacking);
		//para que se mueva en una plataforma
		var displacement = new Vector3(1f, 0f, 0f);
		if (changueMovement)
		{
			//cuando es true el enemigo va hacia la derecha
			var rotation = new Vector3(0f, 180f, 0f);
			transform.position += displacement * speed * Time.deltaTime;
			transform.rotation = Quaternion.Euler(rotation);
		}
		else
		{
			//cuando es false el enemigo va hacia la izquierda
			var rotation = new Vector3(0f, 0f, 0f);
			transform.position -= displacement * speed * Time.deltaTime;
			transform.rotation = Quaternion.Euler(rotation);
		}
		/////////////////////////////////////////////////////////
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "hacha" && player.isAttacking)
		{
			//perder vida cuando player ataca con el hacha
			// Debug.Log(animator.GetFloat("Attack"));
			// if (animator.GetFloat("Attack")<0)
			// {
			lifes--;
			if (lifes == 0)
			{
				Destroy(this.gameObject);
			}
			//  }

			Debug.Log("enemigo melee muere");
		}
		if (other.tag == "shootAttack")
		{
			//perder vida cuando el player te dispara
			lifes--;
			if (lifes == 0)
			{
				Destroy(this.gameObject);
			}
			Debug.Log("enemigo melee muere");
		}
		if (other.tag == "muro_invisible")
		{
			//moverse en una plataforma
			changueMovement = !changueMovement;
			Debug.Log("enemigo melee cambia direccion");
		}
		////////////////////////////////////////////////////////

	}


}