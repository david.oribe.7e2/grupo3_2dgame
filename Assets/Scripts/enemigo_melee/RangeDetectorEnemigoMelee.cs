﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDetectorEnemigoMelee : MonoBehaviour
{
    public EnemigoMeleeController enemigoMelee;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = enemigoMelee.GetComponent<Animator>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.tag=="Player"){
            animator.SetBool("canAttak", true);
            Debug.Log("enemigo melee ha atacado");
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.tag == "Player"){
            animator.SetBool("canAttak", false);
        }
    }
}
