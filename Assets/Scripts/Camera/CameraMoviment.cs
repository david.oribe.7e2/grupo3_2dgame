﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoviment : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float posX = player.transform.position.x;
        float posY = player.transform.position.y;

        transform.position = new Vector3(posX, posY, transform.position.z);
    }
}
