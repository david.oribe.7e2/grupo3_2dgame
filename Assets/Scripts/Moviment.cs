﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moviment : MonoBehaviour
{
    //Public--Methods
    public float maxSpeed = 25f;
    public float speed= 10f;
    public float jumpForce = 1f;
    public bool grounded;

    public bool jump;





    //Private--Methods
    private Rigidbody2D _rg2D;
    private Animator _Anim;
    void Start()
    {
        _rg2D = this.gameObject.GetComponent<Rigidbody2D>();
        //_Anim = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && grounded){
            jump = true;
        }

        //_Anim.SetFloat("Speed",Mathf.Abs(_rg2D.velocity.x));
        //_Anim.SetBool("Grounded",grounded);

        
    }
    void FixedUpdate() {
        //------------Moviment-------------
        float h = Input.GetAxis("Horizontal");
        _rg2D.AddForce(Vector2.right * speed * h);

        //------------LimitMoviment--------
        float limitedSpeed = Mathf.Clamp(_rg2D.velocity.x,-maxSpeed,maxSpeed);
        _rg2D.velocity = new Vector2(limitedSpeed,_rg2D.velocity.y);

        //------------Salto----------------
        if(jump){
            _rg2D.AddForce(Vector2.up * jumpForce);
            jump = false;
        }
    }


}
