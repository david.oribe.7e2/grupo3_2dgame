﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveController : MonoBehaviour
{
    public int life;
    public int speed;
    
    // Start is called before the first frame update
    void Start()
    {
        Animator animator = this.gameObject.GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //para que la nave se mueva
        var displacement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f);
        transform.position += displacement * speed * Time.deltaTime;
        ///////////////////////////////////////////////
    }

}
