﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
public enum GameState{Idle,Playing};

public enum GameLevels{Menu,Level1,Level2,Level3};

//-----------CAMBIAR LOS NOMBRES A EL DE LAS ESCENAS
private string nameMenu = "Menu-Juego";
private string nameEscena1 = "SampleScene";
private string nameEscena2 = "Escena2";
private string nameEscena3 = "Level-Base";

public GameState gameState = GameState.Idle;

public GameLevels gameLevel = GameLevels.Menu;

    // Start is called before the first frame update
    
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CambiarEscena(string nombreEscena){
        SceneManager.LoadScene(nombreEscena);
    }

    public void SalirJuego(){
        Application.Quit();
    }

    public void nextGameLevel(){
        if(gameLevel != GameLevels.Menu){
            gameLevel++;
        }else{
            gameLevel = 0;
        }
        LevelsUpdate();
    }

    public void LevelsUpdate(){
        switch(gameLevel){
            case GameLevels.Menu:
            toMenu();
            break;

            case GameLevels.Level1:
            toLevel1();
            break;

            case GameLevels.Level2:
            toLevel2();
            break;

            case GameLevels.Level3:
            toLevel3();
            break;

            default: 
            gameLevel = GameLevels.Menu;
            break;
        }
    }

    public void toMenu(){
        gameLevel = GameLevels.Menu;
        CambiarEscena(nameMenu);
    }

    public void toLevel1(){
        gameLevel = GameLevels.Level1;
        CambiarEscena(nameEscena1);
    }
    public void toLevel2(){
        gameLevel = GameLevels.Level2;
        CambiarEscena(nameEscena2);
    }
    public void toLevel3(){
        gameLevel = GameLevels.Level3;
        CambiarEscena(nameEscena3);
    }
}
