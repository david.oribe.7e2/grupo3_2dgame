﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChangerController : MonoBehaviour
{
    GameController gameController;
    GameObject _gameObject;

    void Start() {
        _gameObject = GameObject.FindWithTag("GameController");
        gameController = _gameObject.GetComponent<GameController>();
    }
    void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Player"){
            _gameObject.SendMessage("nextGameLevel");
        }
    }
}
