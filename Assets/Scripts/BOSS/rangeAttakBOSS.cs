﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rangeAttakBOSS : MonoBehaviour
{
    public BOSS1Controller boss;
    Animator animator;
    float startHardAttakTime;
    // Start is called before the first frame update
    void Start()
    {
        animator = boss.GetComponent<Animator>();
    }
   
    void Update() {
        startHardAttakTime += Time.deltaTime;
    }
    private void OnTriggerStay2D(Collider2D other) {
        if(other.tag=="Player"){
            animator.SetInteger("action", 1);
            Debug.Log("BOSS ha atacado");
        }
        
        else if(other.tag=="Player" && startHardAttakTime > boss.coolDownHardAttak){
            animator.SetInteger("action", 2);
            Debug.Log("BOSS ha hecho hard attak");
            startHardAttakTime = 0;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.tag == "Player"){
            animator.SetInteger("action", 0);
        }
    }

}
