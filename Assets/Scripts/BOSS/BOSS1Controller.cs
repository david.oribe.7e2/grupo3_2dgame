﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOSS1Controller : MonoBehaviour
{
    public int life;
    public float speed;
	public float coolDownHardAttak;
    public float rangeAttak;
    public GameObject rangeAttakVisual;
    Animator animator;
	

    // Start is called before the first frame update
    void Start()
    {
		animator = this.gameObject.GetComponent<Animator>();

		rangeAttakVisual.GetComponent<CircleCollider2D>().radius = rangeAttak;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	private void OnTriggerEnter2D(Collider2D other)
	{
		switch (other.tag)
		{
			case "hacha":
				//perder vida cuando player ataca con el hacha
				life--;
				if (life == 0)
				{
					Destroy(this.gameObject);
					Debug.Log("BOSS muere");
				}
				break;
			case "shootAttak":
				//perder vida cuando el player te dispara
				life--;
				if (life == 0)
				{
					Destroy(this.gameObject);
					Debug.Log("BOSS muere");
				}
				
				break;
		}
	}
}
