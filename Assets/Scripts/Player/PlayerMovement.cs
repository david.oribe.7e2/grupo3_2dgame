﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int lifes=3;
    public float speed= 10f;
    // Start is called before the first frame update
     [Tooltip("boolean que lo hago true cuando se pulsa W")] 
    private bool jump;

    [Tooltip("Cuanta más fuerza más saltará")] 
    public float jumpForce=6f;

    private Rigidbody2D _rigidbody2D;

    private Animator _animator;

    public float rcDistance = 0f;

    public LayerMask layerGround;

    [Tooltip("VI da del jugador")] 
    public int life=3;

    public Collider2D _collider2DHacha;

    private enum DoAttack{
      normal,
      attacking,
      shooting
    }

    [Tooltip("enum ataques jugador")]
    private DoAttack doAttack;

    [Tooltip("posición inicio disparo")]
    public GameObject launchShootingposition;
        
        
    [Tooltip("OBJETO PROYECTIL")]
    public GameObject shootObject;
    [Tooltip("RATIO DE RECARGA")]
    public float shootRatio=2f;

    [Tooltip("Tiempo entre cada recarga")]
    private float elapsedTime=0f;

    [Tooltip("RATIO DE RECARGA")]
    public float attackRatio = 1f;

    [Tooltip("Tiempo entre cada recarga")]
    private float elapsedAttackTime = 0f;

    public bool isAttacking;
    void Start()
    {
        isAttacking = false;
        doAttack=DoAttack.normal;
        _animator = this.gameObject.GetComponent<Animator>();  

        _rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime+=Time.deltaTime;
        elapsedAttackTime += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.K)&&elapsedTime>shootRatio){
          Instantiate(shootObject, launchShootingposition.transform.position, launchShootingposition.transform.rotation);
          doAttack = DoAttack.shooting;
          elapsedTime=0f;
        }
        float x_movement;
        x_movement = Input.GetAxis("Horizontal");

        //girar
        if (x_movement < 0)
        {
            transform.localScale = new Vector3(-0.25f, 0.25f, 0.25f);
        }
        else
        {
            transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
        }

        doAttackUpdate();
      
      var displacement=new Vector3 (Input.GetAxis("Horizontal"), 0, 0);  
      transform.position += displacement*speed*Time.deltaTime; 
      if (Input.GetKeyDown(KeyCode.W)){
        jump=true;
      }

      Debug.DrawLine(_rigidbody2D.transform.position, Vector2.down, Color.white);
      _animator.SetFloat("Horizontal", Mathf.Abs(x_movement));

      if (Input.GetKeyDown(KeyCode.Space)&&doAttack==DoAttack.normal){
            elapsedAttackTime = 0f;
            doAttack = DoAttack.attacking;
            isAttacking = true;
            //m_attack=true;
        }
        if (elapsedAttackTime > attackRatio)
        {
            isAttacking = false;
        }
       


    }

    private void FixedUpdate() {
        //Salto personaje
        if(jump&&(isOnGround()||isOnActionPlatform())){ //Si se pulsa la tecla W y está tocando el suelo (método isOnGround) podrá saltar
            _rigidbody2D.AddForce(Vector2.up*jumpForce, ForceMode2D.Impulse);
            jump=false;
        }


    }

    bool isOnGround(){
        RaycastHit2D hit= Physics2D.Raycast(transform.position, Vector2.down,rcDistance, layerGround);
        if (hit.collider!=null){
            return true;
        }
        return false;
    }

    bool isOnActionPlatform()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, rcDistance, layerGround);
        if (hit.collider != null)
        {
            return true;
        }
        return false;
    }

    void doAttackUpdate(){
      switch(doAttack){
        case DoAttack.normal:
            _animator.SetFloat("Attack", -1);
            _animator.SetFloat("Shoot", -1);
        break;
        case DoAttack.attacking:
            attack();
        break;
        case DoAttack.shooting:
            shoot();
        break;
        default:
            doAttack=DoAttack.normal;
        break;
          
      }
    }

    private void shoot()
    {
        _animator.SetFloat("Shoot", 1);
        doAttack++;
    }

    private void attack()
    {
        _animator.SetFloat("Attack", 1);
        doAttack++;
    }
}

