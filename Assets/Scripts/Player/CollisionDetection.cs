﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D _rigidbody2D;
    public GameObject _mainCamera;

    PlayerMovement getVariables;

    //public Animator _lifebarAnimator;

    //private float lifes=3f;
    //PlayerMovement playerMovement;

    void Start()
    {
        //playerMovement=this.gameObject.GetComponent<PlayerMovement>();
        _rigidbody2D=this.gameObject.GetComponent<Rigidbody2D>();
        //_lifebarAnimator.SetFloat("lifesNumber", lifes);

    }

    // Update is called once per frame
    void Update()
    {
        getVariables = GetComponent<PlayerMovement>();

    }
    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag=="MainCamera"){
            Debug.Log("You dead camera :(");
            //lifes--;
            _rigidbody2D.transform.position=new Vector3(-17.85f, -9.71f, 0); //El personaje vuelve a la posición inicial
            _mainCamera.transform.position = new Vector3(10f, -1.4f, -10); //Cámara inicial
            getVariables.life--;
            //_lifebarAnimator.SetFloat("lifesNumber", lifes);
        } 
    }
    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag=="enemigo_melee"){
            Debug.Log("You dead :(");
            //_rigidbody2D.transform.position=new Vector3(-17.85f, -9.71f, 0); //El personaje vuelve a la posición inicial
            //_mainCamera.transform.position=new Vector3(10f, -1.4f, -10); //Cámara inicial
            getVariables.life--;

        }
        else if(other.gameObject.tag=="enemigo_a_distancia"){
            //_rigidbody2D.transform.position=new Vector3(-17.85f, -9.71f, 0); //El personaje vuelve a la posición inicial
            //_mainCamera.transform.position=new Vector3(10f, -1.4f, -10); //Cámara inicial
            getVariables.life--;

        }
        else if (other.gameObject.tag == "bala")
        {
            //_rigidbody2D.transform.position = new Vector3(-17.85f, -9.71f, 0); //El personaje vuelve a la posición inicial
            //_mainCamera.transform.position = new Vector3(10f, -1.4f, -10); //Cámara inicial
            getVariables.life--;

        }
    }

    
}
