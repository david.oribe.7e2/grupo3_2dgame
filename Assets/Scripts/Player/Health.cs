﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    // Start is called before the first frame update

    //este playermovement luego le pasaré el objeto a través de la interfaz de unity y cogerá el script automáticamente
    public PlayerMovement getVariables;
    
    SpriteRenderer spriteRenderer;
    public Sprite lifes_3;
    public Sprite lifes_2;
    public Sprite lifes_1;
    public Sprite lifes_0;

    public int lifes;
    void Start()
    {

        Debug.Log(getVariables);
        spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite=lifes_3;
        

    }

    // Update is called once per frame
    void Update()
    {
        //GameObject.Find("MainCharacter").GetComponent<PlayerMovement>().life;
        changeSpriteLifebar();
        //getVariables = GetComponent<PlayerMovement>();
        lifes=getVariables.life;
        Debug.Log(lifes);
    }

    private void changeSpriteLifebar(){
        switch(lifes){
        case 3:
            spriteRenderer.sprite=lifes_3;        
        break;
        case 2:
            spriteRenderer.sprite=lifes_2;        
        break;
        case 1:
            spriteRenderer.sprite=lifes_1;        
        break;
        default:
            spriteRenderer.sprite=lifes_0;
        break;
          
      }
    }
}
