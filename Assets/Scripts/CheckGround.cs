﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    private Moviment player;
    // Start is called before the first frame update
    void Start()
    {
        player = this.gameObject.GetComponent<Moviment>();
    }

    void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Ground"){
            player.grounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D other) {
        if(other.gameObject.tag == "Platform"){
            player.grounded = false;
        }    }

}
