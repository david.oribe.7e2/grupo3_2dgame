﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGeneratorController : MonoBehaviour
{
    public GameObject enemyPrefab;

    public float generatorTimer = 2f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateEnemy(){
        Instantiate(enemyPrefab,transform.position,Quaternion.identity);
    }

    public void ActivareGenerator(){
        InvokeRepeating("CreateEnemy",0f,generatorTimer);
    }

    public void StopGenerator(){
        CancelInvoke("CreateEnemy");
    }
    
}
