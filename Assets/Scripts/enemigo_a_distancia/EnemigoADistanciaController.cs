﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoADistanciaController : MonoBehaviour
{
	public GameObject boca;
	public Transform disparoSpawn;
	public int life;
	public int damage;
	public float fireRate;
	public float rangeAttak;
	public GameObject rangeAttakvisual;
	public GameObject balaReference;
	Animator animator;

	// Start is called before the first frame update
	void Start()
	{
		//establecer un rango de ataque al enemigo
		rangeAttakvisual.GetComponent<CircleCollider2D>().radius = rangeAttak;
		/////////////////////////////////////////

		animator = this.gameObject.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
	{
		
	}
	private void OnTriggerEnter2D(Collider2D other)
	{
		switch (other.tag)
		{
			case "hacha":
				//perder vida cuando player ataca con el hacha
				life--;
				if (life == 0)
				{
					Destroy(this.gameObject);
				}
				Debug.Log("enemigo melee muere");
				break;
			case "shootAttak":
				//perder vida cuando el player te dispara
				life--;
				if (life == 0)
				{
					Destroy(this.gameObject);
				}
				Debug.Log("enemigo melee muere");
				break;

		}
	}
}
