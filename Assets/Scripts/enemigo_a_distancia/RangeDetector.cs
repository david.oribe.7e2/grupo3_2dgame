﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDetector : MonoBehaviour
{
    //variable para hablar todo el rato del script del Enemigo a distancia
    float nextBullet = 0f;
    public EnemigoADistanciaController enemyController;
    Animator animator;
    private void Start() {
        
        animator = enemyController.GetComponent<Animator>();
    }
    //método para saber si el tag Player entra en el rango
    private void OnTriggerStay(Collider other) {
        enemyController = transform.parent.gameObject.GetComponent<EnemigoADistanciaController>();
        if (other.gameObject.tag=="Player"){
            //esta linea hace que el enemigo mire hacia donde esta el player,
            //igual hay que quitarla porque va mal
            enemyController.boca.transform.LookAt(other.transform.position);

            if (Time.time > nextBullet){
                animator.SetBool("canAttak", true);
                Debug.Log("Enemigo a distancia dispara");
                //esto es para instanciar un objeto que sera la bala, desde un punto
                Instantiate(enemyController.balaReference, enemyController.disparoSpawn.position, enemyController.disparoSpawn.rotation);
                //para que se espere a tirar la siguiente bala
                nextBullet = Time.time + enemyController.fireRate;
            }
            else
            {
                animator.SetBool("canAttak", false);
            }
        }
    }
}
